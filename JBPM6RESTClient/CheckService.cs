﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace JBPM6RESTClient
{
    public class CheckService : BaseService
    {
        public CheckService(HttpClient httpClient) : base(httpClient)
        {
        }

        public async Task<bool> isAliveAsync()
        {
            try
            {
                var result = await httpClient.GetAsync("deployment");
                if (result.IsSuccessStatusCode)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
