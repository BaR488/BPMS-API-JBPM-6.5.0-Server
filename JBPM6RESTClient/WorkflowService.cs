﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using JBPM6RESTClient.Models;

namespace JBPM6RESTClient
{
    public class WorkflowService : BaseService
    {
        public WorkflowService(HttpClient httpClient) : base(httpClient)
        {
        }

        public async Task<JbpmProcessDefinitionList> GetEngineWorkflowsAsync()
        {
            var result = await httpClient.GetAsync("deployment/processes");

            if (result.IsSuccessStatusCode)
            {
                return JObject.Parse(result.Content.ReadAsStringAsync().Result).ToObject<JbpmProcessDefinitionList>();
            }
            else
            {
                throw new Exception($"Not SuccessStatusCode: {result.StatusCode}");
            }
        }
        
        public async Task<JbpmProcessDefinition> GetEngineWorkflowAsync(string deploymentId, string processDefId)
        {
            var result = await httpClient.GetAsync($"runtime/{deploymentId}/process/{processDefId}");

            if (result.IsSuccessStatusCode)
            {
                return JObject.Parse(result.Content.ReadAsStringAsync().Result).ToObject<JbpmProcessDefinition>();
            }
            else
            {
                throw new Exception($"Not SuccessStatusCode: {result.StatusCode}");
            }
        }
        
        public async Task<JbpmProcessInstanceResponse> StartEngineWorkflowAsync(string deploymentId, string processDefId, Dictionary<string, string> workflowInstanceVariables)
        {
            
            var requestUrlBuilder = new StringBuilder();
            requestUrlBuilder.Append($"runtime/{deploymentId}/process/{processDefId}/start");
            string delimiter = "?";
            foreach (var pair in workflowInstanceVariables)
            {
                requestUrlBuilder.Append($"{delimiter}map_{pair.Key}={pair.Value}");
                delimiter = "&";
            }
            
            var result = await httpClient.PostAsync(requestUrlBuilder.ToString(), null);
            
            if (result.IsSuccessStatusCode)
            {
                return JObject.Parse(result.Content.ReadAsStringAsync().Result).ToObject<JbpmProcessInstanceResponse>();
            }
            else
            {
                throw new Exception($"Not SuccessStatusCode: {result.StatusCode}", new Exception(result.ReasonPhrase));
            }
        }
                
        public async Task<JbpmResponse> AbortEngineWorkflowAsync(string deploymentId, string processInstanceId)
        {          
            var result = await httpClient.PostAsync($"runtime/{deploymentId}/process/instance/{processInstanceId}/abort", null);
            
            if (result.IsSuccessStatusCode)
            {
                return JObject.Parse(result.Content.ReadAsStringAsync().Result).ToObject<JbpmResponse>();
            }
            else
            {
                throw new Exception($"Not SuccessStatusCode: {result.StatusCode}", new Exception(result.ReasonPhrase));
            }
        }
    }
}