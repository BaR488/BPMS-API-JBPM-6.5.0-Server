﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using JBPM6RESTClient.Models;
using JBPM6RESTClient.Models.JbpmQueryProcessInstanceResult;
using JBPM6RESTClient.Utils;
using Newtonsoft.Json.Linq;

namespace JBPM6RESTClient
{
    public class WorkflowInstanceService : BaseService
    {
        public WorkflowInstanceService(HttpClient httpClient, HttpClient xmlHttpClient) : base(httpClient, xmlHttpClient)
        {
        }

        public WorkflowInstanceService(HttpClient httpClient) : base(httpClient)
        {
        }
        
        public async Task<JbpmQueryProcessInstanceResult> GetEngineWorkflowInstancesAsync(string processDefId, int? pageNum, int? pageSize)
        {
            var result = await httpClient.GetAsync($"query/runtime/process?processid={processDefId}&pageSize={pageSize}&page={pageNum}");

            if (result.IsSuccessStatusCode)
            {
                return JObject.Parse(result.Content.ReadAsStringAsync().Result).ToObject<JbpmQueryProcessInstanceResult>();
            }
            else
            {
                throw new Exception($"Not SuccessStatusCode: {result.StatusCode}");
            }
        }
        
        public async Task<JbpmProcessInstanceInfo> GetEngineWorkflowInstanceAsync(string processDefId, string processInstanceId)
        {
            var result = await httpClient.GetAsync($"query/runtime/process?processid={processDefId}&processinstanceid={processInstanceId}");

            if (result.IsSuccessStatusCode)
            {
                return JObject.Parse(result.Content.ReadAsStringAsync().Result).ToObject<JbpmQueryProcessInstanceResult>().ProcessInstanceInfoList.FirstOrDefault();
            }
            else
            {
                throw new Exception($"Not SuccessStatusCode: {result.StatusCode}");
            }
        }
        
        public async Task<int> GetEngineWorkflowInstancesTotalCountAsync(string processDefId)
        {
            var result = await httpClient.GetAsync($"query/runtime/process?processid={processDefId}");

            if (result.IsSuccessStatusCode)
            {
                return JObject.Parse(result.Content.ReadAsStringAsync().Result).ToObject<JbpmQueryProcessInstanceResult>().ProcessInstanceInfoList.Count;
            }
            else
            {
                throw new Exception($"Not SuccessStatusCode: {result.StatusCode}");
            }
        }

        public async Task<bool> SetProcessInstanceVariablesCommand(string deploymentId, string instanceId, string variableId, string workflowInstanceValiableValue)
        {
            var setProcessInstanceVariablesCommand = new SetProcessInstanceVariablesCommand();

            
            
            setProcessInstanceVariablesCommand.DeploymentId = deploymentId;
            setProcessInstanceVariablesCommand.processInstanceVars = new SetProcessInstanceVariablesCommand.SetProcessInstanceVars
            {
                ProcessInstanceId = instanceId,
                Variables = new SetProcessInstanceVariablesCommand.Variables
                {
                    Item = new SetProcessInstanceVariablesCommand.Item
                    {
                        Key = variableId,
                        Value = new SetProcessInstanceVariablesCommand.Value
                        {
                            Text = workflowInstanceValiableValue,
                            Type = "xs:string",
                            Xs = "http://www.w3.org/2001/XMLSchema",
                            Xsi = "http://www.w3.org/2001/XMLSchema-instance"
                        }
                    }
                }
            };
                                
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(SetProcessInstanceVariablesCommand));

            using(Utf8StringWriter textWriter = new Utf8StringWriter())
            {
                xmlSerializer.Serialize(textWriter, setProcessInstanceVariablesCommand);              
                
                var httpContent = new StringContent(textWriter.ToString(), Encoding.UTF8, "application/xml");
                        
                var result = await xmlHttpClient.PostAsync($"execute", httpContent);
            
                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    throw new Exception($"Not SuccessStatusCode: {result.StatusCode}");
                }
            }
        }
    }
}