﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace JBPM6RESTClient
{
    public class BaseService
    {
        protected readonly HttpClient httpClient;
        protected readonly HttpClient xmlHttpClient;

        public BaseService(HttpClient httpClient, HttpClient xmlHttpClient)
        {
            this.httpClient = httpClient;
            this.xmlHttpClient = xmlHttpClient;
        }

        public BaseService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }
    }
}
