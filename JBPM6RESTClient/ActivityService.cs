﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using JBPM6RESTClient.Models;
using JBPM6RESTClient.Models.JbpmWorkitem;
using Newtonsoft.Json.Linq;

namespace JBPM6RESTClient
{
    public class ActivityService : BaseService
    {
        public ActivityService(HttpClient httpClient, HttpClient xmlHttpClient) : base(httpClient, xmlHttpClient)
        {
        }

        public ActivityService(HttpClient httpClient) : base(httpClient)
        {
        }

        public async Task<JbpmResponse> CompleteActivity(string deploymentId, string activityId, Dictionary<string, string> results)
        {
            
            var requestUrlBuilder = new StringBuilder();
            requestUrlBuilder.Append($"runtime/{deploymentId}/workitem/{activityId}/complete");
            string delimiter = "?";
            foreach (var pair in results)
            {
                requestUrlBuilder.Append($"{delimiter}map_{pair.Key}={pair.Value}");
                delimiter = "&";
            }
                       
            var request = await httpClient.PostAsync(requestUrlBuilder.ToString(), null);
            
            if (request.IsSuccessStatusCode)
            {
                return JObject.Parse(request.Content.ReadAsStringAsync().Result).ToObject<JbpmResponse>();
            }
            else
            {
                throw new Exception($"Not SuccessStatusCode: {request.StatusCode}", new Exception(request.ReasonPhrase));
            }
        }
        
        public async Task<JbpmWorkitemResult> GetActivity(string deploymentId, string activityId)
        {           
            var request = await httpClient.GetAsync($"runtime/{deploymentId}/workitem/{activityId}");
            
            if (request.IsSuccessStatusCode)
            {
                return JObject.Parse(request.Content.ReadAsStringAsync().Result).ToObject<JbpmWorkitemResult>();
            }
            else
            {
                throw new Exception($"Not SuccessStatusCode: {request.StatusCode}", new Exception(request.ReasonPhrase));
            }
        }
    }
}