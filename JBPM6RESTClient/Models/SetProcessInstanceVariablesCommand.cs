﻿using System.Xml.Serialization;

namespace JBPM6RESTClient.Models
{
    [XmlRoot(ElementName="command-request")]
    public class SetProcessInstanceVariablesCommand
    {
        [XmlElement(ElementName="deployment-id")]
        public string DeploymentId { get; set; }
        [XmlElement(ElementName="set-process-instance-vars")]
        public SetProcessInstanceVars processInstanceVars { get; set; }                
        
        [XmlRoot(ElementName="value")]
        public class Value {
            [XmlAttribute(AttributeName="type", Namespace="http://www.w3.org/2001/XMLSchema-instance")]
            public string Type { get; set; }
            [XmlAttribute(AttributeName="xs", Namespace="http://www.w3.org/2000/xmlns/")]
            public string Xs { get; set; }
            [XmlAttribute(AttributeName="xsi", Namespace="http://www.w3.org/2000/xmlns/")]
            public string Xsi { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName="item")]
        public class Item {
            [XmlElement(ElementName="value")]
            public Value Value { get; set; }
            [XmlAttribute(AttributeName="key")]
            public string Key { get; set; }
        }

        [XmlRoot(ElementName="variables")]
        public class Variables {
            [XmlElement(ElementName="item")]
            public Item Item { get; set; }
        }

        [XmlRoot(ElementName="set-process-instance-vars")]
        public class SetProcessInstanceVars {
            [XmlElement(ElementName="variables")]
            public Variables Variables { get; set; }
            [XmlAttribute(AttributeName="processInstanceId")]
            public string ProcessInstanceId { get; set; }
        }
    }
}