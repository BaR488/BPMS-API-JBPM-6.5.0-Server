﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace JBPM6RESTClient.Models.JbpmQueryProcessInstanceResult
{
    public class JbpmProcessInstanceInfo
    {
        [JsonProperty("variables")]
        public List<JaxbVariableInfo> Variables { get; set; }

        [JsonProperty("process-instance")]
        public JbpmProcessInstance ProcessInstance { get; set; }
    }
}