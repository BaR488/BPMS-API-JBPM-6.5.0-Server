﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace JBPM6RESTClient.Models.JbpmQueryProcessInstanceResult
{
    public class JbpmProcessInstance
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("state")]
        public JbpmProcessInstanceState State { get; set; }

        [JsonProperty("parentProcessInstanceId")]
        public long ParentProcessInstanceId { get; set; }

        [JsonProperty("process-id")]
        public string ProcessId { get; set; }

        [JsonProperty("event-types")]
        public List<object> EventTypes { get; set; }
    }
}