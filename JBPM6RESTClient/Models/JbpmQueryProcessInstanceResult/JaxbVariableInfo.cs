﻿using Newtonsoft.Json;

namespace JBPM6RESTClient.Models.JbpmQueryProcessInstanceResult
{
    public class JaxbVariableInfo
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public object Value { get; set; }

        [JsonProperty("lastModificationDate")]
        public long LastModificationDate { get; set; }
    }
}