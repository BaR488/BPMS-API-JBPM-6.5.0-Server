﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace JBPM6RESTClient.Models.JbpmQueryProcessInstanceResult
{
    public class JbpmQueryProcessInstanceResult
    {
        [JsonProperty("processInstanceInfoList")]
        public List<JbpmProcessInstanceInfo> ProcessInstanceInfoList { get; set; }
    }
}