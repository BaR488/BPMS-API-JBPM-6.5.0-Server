﻿namespace JBPM6RESTClient.Models.JbpmWorkitem
{
    public enum WorkItemState
    {
        Pending = 0,
        Active = 1,
        Completed = 2,
        Aborted = 3
    }
}