﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JBPM6RESTClient.Models.JbpmWorkitem
{
    public class JbpmWorkitemResult
    {
        [JsonProperty("index")]
        public object Index { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("state")]
        public WorkItemState State { get; set; }

        [JsonProperty("processInstanceId")]
        public long ProcessInstanceId { get; set; }

        [JsonProperty("param-map")]
        public JObject ParamMap { get; set; }

        [JsonProperty("results-map")]
        public JObject ResultsMap { get; set; }
    }
}