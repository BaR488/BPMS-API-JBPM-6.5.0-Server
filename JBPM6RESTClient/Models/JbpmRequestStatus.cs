﻿using System.Runtime.Serialization;

namespace JBPM6RESTClient.Models
{
    public enum JaxbRequestStatus { 
        [EnumMember(Value = "SUCCESS")]
        Success, 
        // Technical failure on the server side 
        [EnumMember(Value = "FAILURE")]
        Failure, 
        // Syntax exception or command not accepted 
        [EnumMember(Value = "BAD_REQUEST")]
        BadRequest, 
        // not an allowed command 
        [EnumMember(Value = "FORBIDDEN")]
        Forbidden, 
        // task service permissions 
        [EnumMember(Value = "PERMISSIONS_CONFLICT")]
        PermissionsConflict, 
        // instance does not exist 
        [EnumMember(Value = "NOT_FOUND")]
        NotFound 
    }
}