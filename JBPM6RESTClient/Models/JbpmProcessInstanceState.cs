﻿namespace JBPM6RESTClient.Models
{
    public enum JbpmProcessInstanceState
    {
        StatePending = 0,
        StateActive = 1,
        StateCompleted = 2,
        StateAborted = 3,
        StateSuspended = 4
    }
}