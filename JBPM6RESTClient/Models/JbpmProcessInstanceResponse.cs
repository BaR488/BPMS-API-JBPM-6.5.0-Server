﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace JBPM6RESTClient.Models
{
    public partial class JbpmProcessInstanceResponse
    {
        [JsonProperty("id")] public long Id { get; set; }

        [JsonProperty("state")] public JbpmProcessInstanceState State { get; set; }

        [JsonProperty("parentProcessInstanceId")]
        public long ParentProcessInstanceId { get; set; }

        [JsonProperty("status")] public JaxbRequestStatus Status { get; set; }

        [JsonProperty("url")] public string Url { get; set; }

        [JsonProperty("index")] public object Index { get; set; }

        [JsonProperty("process-id")] public string ProcessId { get; set; }

        [JsonProperty("event-types")] public List<object> EventTypes { get; set; }
    }
}