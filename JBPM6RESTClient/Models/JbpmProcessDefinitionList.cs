﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace JBPM6RESTClient.Models
{
    public partial class JbpmProcessDefinitionList
    {
        [JsonProperty("processDefinitionList")]
        public List<ProcessDefinitionList> ProcessDefinitionList { get; set; }
    }
    
    public partial class ProcessDefinitionList
    {
        [JsonProperty("process-definition")]
        public JbpmProcessDefinition ProcessDefinition { get; set; }
    }

}