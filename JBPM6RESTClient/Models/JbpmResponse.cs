﻿using Newtonsoft.Json;

namespace JBPM6RESTClient.Models
{
    public class JbpmResponse
    {
        [JsonProperty("status")]
        public JaxbRequestStatus Status { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}