﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace JBPM6RESTClient.Models
{
    public class JbpmProcessDefinition
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("forms")]
        public object Forms { get; set; }

        [JsonProperty("variables")]
        public Dictionary<string, string> Variables { get; set; }

        [JsonProperty("package-name")]
        public string PackageName { get; set; }

        [JsonProperty("deployment-id")]
        public string DeploymentId { get; set; }
    }
}