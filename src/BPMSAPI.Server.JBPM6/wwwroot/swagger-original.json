{
  "swagger" : "2.0",
  "info" : {
    "description" : "Универсальный интерфейс взаимодействия с различными BPM системами.",
    "version" : "1.0.0",
    "title" : "BPMS API",
    "license" : {
      "name" : "MIT",
      "url" : "http://opensource.org/licenses/MIT"
    }
  },
  "host" : "localhost:5000",
  "basePath" : "/api",
  "tags" : [ {
    "name" : "Common",
    "description" : "Oбщая информация"
  }, {
    "name" : "Workflows",
    "description" : "Процессы"
  }, {
    "name" : "WorkflowInstances",
    "description" : "Экземпляры процессов"
  }, {
    "name" : "WorkflowInstanceVariables",
    "description" : "Переменные экземпляра процесса"
  }, {
    "name" : "Activities",
    "description" : "Активности"
  }, {
    "name" : "Actions",
    "description" : "Действия"
  } ],
  "schemes" : [ "http", "https" ],
  "paths" : {
    "/info" : {
      "get" : {
        "tags" : [ "Common" ],
        "summary" : "Получить информацию о СУБП.",
        "operationId" : "GetBPMSInfo",
        "produces" : [ "application/json" ],
        "parameters" : [ ],
        "responses" : {
          "200" : {
            "description" : "OK",
            "schema" : {
              "$ref" : "#/definitions/BPMSInfo"
            }
          }
        }
      }
    },
    "/workflows" : {
      "get" : {
        "tags" : [ "Workflows" ],
        "summary" : "Получение списка доступных процессов СУБП",
        "operationId" : "GetEngineWorkflows",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "pageNum",
          "in" : "query",
          "description" : "Номер страницы",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        }, {
          "name" : "pageSize",
          "in" : "query",
          "description" : "Размер страницы",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "OK",
            "schema" : {
              "$ref" : "#/definitions/WorkflowList"
            }
          }
        }
      }
    },
    "/workflows/{workflowId}" : {
      "get" : {
        "tags" : [ "Workflows" ],
        "summary" : "Получение конкретного процесса.",
        "operationId" : "GetEngineWorkflow",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "workflowId",
          "in" : "path",
          "description" : "Идентификатор процесса",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "OK",
            "schema" : {
              "$ref" : "#/definitions/Workflow"
            }
          },
          "204" : {
            "description" : "Процесс не найден",
            "schema" : {
              "$ref" : "#/definitions/Error"
            }
          }
        }
      }
    },
    "/workflows/{workflowId}/instances/start" : {
      "post" : {
        "tags" : [ "Actions" ],
        "summary" : "Запуск экземпляра процесса",
        "operationId" : "StartWorkflowInstance",
        "parameters" : [ {
          "name" : "workflowId",
          "in" : "path",
          "description" : "Идентификатор процесса",
          "required" : true,
          "type" : "string"
        }, {
          "in" : "body",
          "name" : "workflowInstanceVariables",
          "description" : "Переменные процесса",
          "required" : true,
          "schema" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/definitions/WorkflowInstanceVariable"
            }
          }
        } ],
        "responses" : {
          "201" : {
            "description" : "Процесс запущен",
            "schema" : {
              "$ref" : "#/definitions/WorkflowInstance"
            }
          },
          "204" : {
            "description" : "Процесс не найден",
            "schema" : {
              "$ref" : "#/definitions/Error"
            }
          },
          "400" : {
            "description" : "Ошибка запуска"
          }
        }
      }
    },
    "/workflows/{workflowId}/instances" : {
      "get" : {
        "tags" : [ "WorkflowInstances" ],
        "summary" : "Получение списка экземпляров процессов.",
        "operationId" : "GetEngineWorkflowInstances",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "workflowId",
          "in" : "path",
          "description" : "Идентификатор процесса",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "pageNum",
          "in" : "query",
          "description" : "Номер страницы",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        }, {
          "name" : "pageSize",
          "in" : "query",
          "description" : "Размер страницы",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "OK",
            "schema" : {
              "$ref" : "#/definitions/WorkflowInstanceList"
            }
          }
        }
      }
    },
    "/workflows/{workflowId}/instances/{instanceId}" : {
      "get" : {
        "tags" : [ "WorkflowInstances" ],
        "summary" : "Получение экземпляра процесса.",
        "operationId" : "GetEngineWorkflowInstance",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "workflowId",
          "in" : "path",
          "description" : "Идентификатор процесса",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "instanceId",
          "in" : "path",
          "description" : "Идентификатор экземпляра процесса",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "OK",
            "schema" : {
              "$ref" : "#/definitions/WorkflowInstanceFull"
            }
          }
        }
      }
    },
    "/workflows/{workflowId}/instances/{instanceId}/variables" : {
      "get" : {
        "tags" : [ "WorkflowInstanceVariables" ],
        "summary" : "Получение списка переменных экземпляра процесса.",
        "operationId" : "GetEngineWorkflowInstanceVariables",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "workflowId",
          "in" : "path",
          "description" : "Идентификатор процесса",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "instanceId",
          "in" : "path",
          "description" : "Идентификатор экземпляра процесса",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "pageNum",
          "in" : "query",
          "description" : "Номер страницы",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        }, {
          "name" : "pageSize",
          "in" : "query",
          "description" : "Размер страницы",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "Список переменных экземпляра процесса.",
            "schema" : {
              "$ref" : "#/definitions/WorkflowInstanceVariableList"
            }
          }
        }
      },
      "post" : {
        "tags" : [ "WorkflowInstanceVariables" ],
        "summary" : "Добавление переменной в экземпляр процесса.",
        "operationId" : "AddEngineWorkflowInstanceVariable",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "workflowId",
          "in" : "path",
          "description" : "Идентификатор процесса",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "instanceId",
          "in" : "path",
          "description" : "Идентификатор экземпляра процесса",
          "required" : true,
          "type" : "string"
        }, {
          "in" : "body",
          "name" : "workflowInstanceVariable",
          "description" : "Переменная процесса",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/WorkflowInstanceVariable"
          }
        } ],
        "responses" : {
          "201" : {
            "description" : "Переменная добавлена в процесс"
          },
          "409" : {
            "description" : "Переменная уже существует"
          }
        }
      }
    },
    "/workflows/{workflowId}/instances/{instanceId}/variables/{variableId}" : {
      "get" : {
        "tags" : [ "WorkflowInstanceVariables" ],
        "summary" : "Получение значения переменной экземпляра процесса.",
        "operationId" : "GetEngineWorkflowInstanceVariableValue",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "workflowId",
          "in" : "path",
          "description" : "Идентификатор процесса",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "instanceId",
          "in" : "path",
          "description" : "Идентификатор экземпляра процесса",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "variableId",
          "in" : "path",
          "description" : "Идентификатор переменной экземпляра процесса",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "Значение переменной экземпляра процесса.",
            "schema" : {
              "$ref" : "#/definitions/WorkflowInstanceVariable"
            }
          }
        }
      },
      "put" : {
        "tags" : [ "WorkflowInstanceVariables" ],
        "summary" : "Изменение переменной в экземпляре процесса.",
        "operationId" : "SetEngineWorkflowInstanceVariable",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "workflowId",
          "in" : "path",
          "description" : "Идентификатор процесса",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "instanceId",
          "in" : "path",
          "description" : "Идентификатор экземпляра процесса",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "variableId",
          "in" : "path",
          "description" : "Идентификатор переменной экземпляра процесса",
          "required" : true,
          "type" : "string"
        }, {
          "in" : "body",
          "name" : "workflowInstanceValiableValue",
          "description" : "Значение переменной процесса",
          "required" : true,
          "schema" : {
            "type" : "string"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "Переменная добавлена в процесс"
          },
          "204" : {
            "description" : "Переменная не существует"
          }
        }
      }
    },
    "/workflows/{workflowId}/instances/{instanceId}/abort" : {
      "post" : {
        "tags" : [ "Actions" ],
        "summary" : "Остановка экземпляра процесса.",
        "operationId" : "StopWorkflowInstance",
        "parameters" : [ {
          "name" : "workflowId",
          "in" : "path",
          "description" : "Идентификатор процесса",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "instanceId",
          "in" : "path",
          "description" : "Идентификатор экземпляра процесса",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "Процесс остановлен"
          },
          "204" : {
            "description" : "Процесс не найден"
          }
        }
      }
    },
    "/workflows/{workflowId}/instances/{instanceId}/activities" : {
      "get" : {
        "tags" : [ "Activities" ],
        "summary" : "Получение списка активностей экземпляра процесса.",
        "operationId" : "GetEngineWorkflowInstanceActivities",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "workflowId",
          "in" : "path",
          "description" : "Идентификатор процесса",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "instanceId",
          "in" : "path",
          "description" : "Идентификатор экземпляра процесса",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "pageNum",
          "in" : "query",
          "description" : "Номер страницы",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        }, {
          "name" : "pageSize",
          "in" : "query",
          "description" : "Размер страницы",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "Список активностей экземпляра процесса.",
            "schema" : {
              "$ref" : "#/definitions/ActivityList"
            }
          }
        }
      }
    },
    "/workflows/{workflowId}/instances/{instanceId}/activities/{activityId}" : {
      "get" : {
        "tags" : [ "Activities" ],
        "summary" : "Получение активности по идентификатору",
        "operationId" : "GetEngineWorkflowInstanceActivity",
        "parameters" : [ {
          "name" : "activityId",
          "in" : "path",
          "description" : "Идентификатор активности",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "workflowId",
          "in" : "path",
          "description" : "Идентификатор процесса",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "instanceId",
          "in" : "path",
          "description" : "Идентификатор экземпляра процесса",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "Активность экземпляра процесса.",
            "schema" : {
              "$ref" : "#/definitions/ActivityFull"
            }
          }
        }
      }
    },
    "/workflows/{workflowId}/instances/{instanceId}/activities/{activityId}/complete" : {
      "post" : {
        "tags" : [ "Actions" ],
        "summary" : "Завершение задачи с результатом.",
        "operationId" : "CompleteActivity",
        "parameters" : [ {
          "name" : "activityId",
          "in" : "path",
          "description" : "Идентификатор активности",
          "required" : true,
          "type" : "string"
        }, {
          "in" : "body",
          "name" : "activityResults",
          "description" : "Результаты активаности",
          "required" : true,
          "schema" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/definitions/DictionaryItem"
            }
          }
        }, {
          "name" : "workflowId",
          "in" : "path",
          "description" : "Идентификатор процесса",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "instanceId",
          "in" : "path",
          "description" : "Идентификатор экземпляра процесса",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "Активность завершена."
          },
          "204" : {
            "description" : "Активность не существует."
          }
        }
      }
    }
  },
  "definitions" : {
    "BaseList" : {
      "type" : "object",
      "required" : [ "_meta" ],
      "properties" : {
        "_meta" : {
          "$ref" : "#/definitions/BaseList__meta"
        }
      }
    },
    "DictionaryItem" : {
      "type" : "object",
      "required" : [ "id", "value" ],
      "properties" : {
        "id" : {
          "type" : "string"
        },
        "value" : {
          "type" : "string"
        }
      }
    },
    "BPMSInfo" : {
      "type" : "object",
      "required" : [ "status", "type", "version" ],
      "properties" : {
        "type" : {
          "type" : "string"
        },
        "version" : {
          "type" : "string"
        },
        "status" : {
          "type" : "string",
          "enum" : [ "Active", "Inactive" ]
        }
      },
      "example" : {
        "type" : "JBPM",
        "version" : "6.5.0",
        "status" : "Active"
      }
    },
    "Error" : {
      "type" : "object",
      "required" : [ "message", "type" ],
      "properties" : {
        "type" : {
          "type" : "string",
          "enum" : [ "NetworkError", "EngineError", "CustomError" ]
        },
        "message" : {
          "type" : "string"
        }
      },
      "example" : {
        "type" : "NetworkError",
        "message" : "Connection timed out"
      }
    },
    "Workflow" : {
      "type" : "object",
      "required" : [ "id" ],
      "properties" : {
        "id" : {
          "type" : "string"
        },
        "version" : {
          "type" : "string"
        },
        "name" : {
          "type" : "string"
        }
      },
      "example" : {
        "id" : "fd716138-a5df-44a9-b2f3-866790ae15e6",
        "version" : "1.0.0",
        "name" : "Оформление отпуска"
      }
    },
    "WorkflowList" : {
      "allOf" : [ {
        "$ref" : "#/definitions/BaseList"
      }, {
        "properties" : {
          "items" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/definitions/Workflow"
            }
          }
        }
      } ]
    },
    "WorkflowInstanceVariable" : {
      "allOf" : [ {
        "$ref" : "#/definitions/DictionaryItem"
      } ]
    },
    "WorkflowInstanceVariableList" : {
      "allOf" : [ {
        "$ref" : "#/definitions/BaseList"
      }, {
        "properties" : {
          "items" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/definitions/WorkflowInstanceVariable"
            }
          }
        }
      } ]
    },
    "WorkflowInstanceFull" : {
      "allOf" : [ {
        "$ref" : "#/definitions/WorkflowInstance"
      }, {
        "properties" : {
          "variebles" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/definitions/WorkflowInstanceVariable"
            }
          }
        }
      } ]
    },
    "WorkflowInstance" : {
      "type" : "object",
      "required" : [ "id", "state", "workflowId" ],
      "properties" : {
        "id" : {
          "type" : "string"
        },
        "workflowId" : {
          "type" : "string"
        },
        "state" : {
          "$ref" : "#/definitions/WorkflowInstanceState"
        }
      },
      "example" : {
        "id" : "fd716138-a5df-44a9-b2f3-866790ae15e6",
        "workflowId" : "fd716138-a5df-44a9-b2f3-866790ae15e6",
        "state" : "Running"
      }
    },
    "WorkflowInstanceList" : {
      "allOf" : [ {
        "$ref" : "#/definitions/BaseList"
      }, {
        "properties" : {
          "items" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/definitions/WorkflowInstance"
            }
          }
        }
      } ]
    },
    "ActivityFull" : {
      "allOf" : [ {
        "$ref" : "#/definitions/Activity"
      }, {
        "properties" : {
          "parameters" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/definitions/DictionaryItem"
            }
          },
          "results" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/definitions/DictionaryItem"
            }
          }
        }
      } ]
    },
    "Activity" : {
      "type" : "object",
      "required" : [ "id", "processInstanceId" ],
      "properties" : {
        "id" : {
          "type" : "string"
        },
        "name" : {
          "type" : "string"
        },
        "processInstanceId" : {
          "type" : "string"
        },
        "state" : {
          "$ref" : "#/definitions/ActivityState"
        }
      }
    },
    "ActivityList" : {
      "allOf" : [ {
        "$ref" : "#/definitions/BaseList"
      }, {
        "properties" : {
          "items" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/definitions/Activity"
            }
          }
        }
      } ]
    },
    "WorkflowInstanceState" : {
      "type" : "object",
      "required" : [ "workflowInstanceStateValue" ],
      "properties" : {
        "workflowInstanceStateValue" : {
          "type" : "string",
          "enum" : [ "Created", "Starting", "Running", "Faulted", "Finished", "Cancelling", "Cancelled" ]
        }
      }
    },
    "ActivityState" : {
      "type" : "object",
      "required" : [ "activityStateValue" ],
      "properties" : {
        "activityStateValue" : {
          "type" : "string",
          "enum" : [ "Pending", "Active", "Completed", "Aborted" ]
        }
      }
    },
    "BaseList__meta" : {
      "required" : [ "pageNum", "totalPages", "totalRecordCount" ],
      "properties" : {
        "pageNum" : {
          "type" : "integer",
          "format" : "int32",
          "default" : 1
        },
        "pageSize" : {
          "type" : "integer",
          "format" : "int32"
        },
        "totalPages" : {
          "type" : "integer",
          "format" : "int32",
          "default" : 1
        },
        "totalRecordCount" : {
          "type" : "integer",
          "format" : "int32",
          "default" : 0
        }
      }
    }
  }
}
