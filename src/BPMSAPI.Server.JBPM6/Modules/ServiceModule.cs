﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using JBPM6RESTClient;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BPMSAPI.Server.JBPM6.Modules
{
    public class ServiceModule : Module
    {
        private readonly IServiceCollection _services;

        public ServiceModule()
        {
            _services = new ServiceCollection();
        }

        protected override void Load(ContainerBuilder builder)
        {
            var jbpmUrl = Environment.GetEnvironmentVariable("jbpmUrl");
            var user = Environment.GetEnvironmentVariable("user");
            var pass = Environment.GetEnvironmentVariable("pass");

            if (string.IsNullOrEmpty(jbpmUrl))
                throw new Exception("Url для подключения к jBpm не задан");
            if (string.IsNullOrEmpty(user))
                throw new Exception("Логин для подключения к jBpm не задан");
            if (string.IsNullOrEmpty(pass))
                throw new Exception("Пароль для подключения к jBpm не задан");

            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(jbpmUrl);
            var byteArray = Encoding.ASCII.GetBytes($"{user}:{pass}");
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var xmlHttpClient = new HttpClient();
            xmlHttpClient.BaseAddress = new Uri(jbpmUrl);
            xmlHttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            xmlHttpClient.DefaultRequestHeaders.Accept.Clear();

            builder.RegisterType<WorkflowService>()
                .As<WorkflowService>()
                .WithParameter("httpClient", httpClient);
            
            builder.RegisterType<CheckService>()
                .As<CheckService>()
                .WithParameter("httpClient", httpClient);
            
            builder.RegisterType<WorkflowInstanceService>()
                .As<WorkflowInstanceService>()
                .WithParameter("httpClient", httpClient)
                .WithParameter("xmlHttpClient", xmlHttpClient);

            builder.RegisterType<ActivityService>()
                .As<ActivityService>()
                .WithParameter("httpClient", httpClient);

            builder.Populate(_services);
        }
    }
}
