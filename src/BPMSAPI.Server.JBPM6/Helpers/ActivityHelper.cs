﻿using BPMSAPI.Server.JBPM6.Models;
using JBPM6RESTClient.Models;
using JBPM6RESTClient.Models.JbpmWorkitem;

namespace BPMSAPI.Server.JBPM6.Helpers
{
    public class ActivityHelper
    {
        public static ActivityState StateParse(WorkItemState resultState)
        {
            ActivityState.ActivityStateValueEnum state = ActivityState.ActivityStateValueEnum.PendingEnum;
            switch (resultState)
            {
                case WorkItemState.Aborted:
                    state = ActivityState.ActivityStateValueEnum.AbortedEnum;
                    break;
                case WorkItemState.Active:
                    state = ActivityState.ActivityStateValueEnum.ActiveEnum;
                    break;
                case WorkItemState.Completed:
                    state = ActivityState.ActivityStateValueEnum.CompletedEnum;
                    break;
                case WorkItemState.Pending:
                    state = ActivityState.ActivityStateValueEnum.PendingEnum;
                    break;
            }

            return new ActivityState
            {
                ActivityStateValue = state
            };
        }
    }
}