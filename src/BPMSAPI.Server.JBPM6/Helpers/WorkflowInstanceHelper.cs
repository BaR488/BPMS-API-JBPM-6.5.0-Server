﻿using System.Collections.Generic;
using System.Linq;
using BPMSAPI.Server.JBPM6.Models;
using JBPM6RESTClient.Models;
using JBPM6RESTClient.Models.JbpmQueryProcessInstanceResult;
using Newtonsoft.Json.Linq;

namespace BPMSAPI.Server.JBPM6.Helpers
{
    public class WorkflowInstanceHelper
    {
        public static WorkflowInstanceState StateParse(JbpmProcessInstanceState resultState)
        {
            WorkflowInstanceState.WorkflowInstanceStateValueEnum state;
            switch (resultState)
            {
                case JbpmProcessInstanceState.StateAborted:
                    state = WorkflowInstanceState.WorkflowInstanceStateValueEnum.FaultedEnum;
                    break;
                case JbpmProcessInstanceState.StateActive:
                    state = WorkflowInstanceState.WorkflowInstanceStateValueEnum.RunningEnum;
                    break;
                case JbpmProcessInstanceState.StateCompleted:
                    state = WorkflowInstanceState.WorkflowInstanceStateValueEnum.FinishedEnum;
                    break;
                case JbpmProcessInstanceState.StatePending:
                    state = WorkflowInstanceState.WorkflowInstanceStateValueEnum.CreatedEnum;
                    break;
                case JbpmProcessInstanceState.StateSuspended:
                    state = WorkflowInstanceState.WorkflowInstanceStateValueEnum.StartingEnum;
                    break;
                default:
                    state = WorkflowInstanceState.WorkflowInstanceStateValueEnum.CreatedEnum;
                    break;
            }

            return new WorkflowInstanceState
            {
                WorkflowInstanceStateValue = state
            };
        }

        public static WorkflowInstance WorkflowInstanceParse(JbpmProcessInstanceInfo jbpmProcessInstanceInfo)
        {
            return new WorkflowInstance
            {
                Id = jbpmProcessInstanceInfo.ProcessInstance.Id.ToString(),
                State = StateParse(jbpmProcessInstanceInfo.ProcessInstance.State)
            };
        }

        public static WorkflowInstanceFull WorkflowInstanceFullParse(JbpmProcessInstanceInfo jBpmWorkflowsInstance)
        {
            return new WorkflowInstanceFull
            {
                Id = jBpmWorkflowsInstance.ProcessInstance.Id.ToString(),
                State = StateParse(jBpmWorkflowsInstance.ProcessInstance.State),
                Variebles = jBpmWorkflowsInstance.Variables.Select(WorkflowInstanceVariableParse).ToList()
            };
        }
        
        public static WorkflowInstanceVariable WorkflowInstanceVariableParse(JaxbVariableInfo jBpmWorkflowsInstance)
        {
            return new WorkflowInstanceVariable
            {
                Id = jBpmWorkflowsInstance.Name,
                Value = JObject.FromObject(jBpmWorkflowsInstance.Value)["value"].ToString()
            };
        }
    }
}