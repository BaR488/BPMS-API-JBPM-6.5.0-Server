﻿using BPMSAPI.Server.JBPM6.Models;
using JBPM6RESTClient.Models;
using Newtonsoft.Json.Linq;

namespace BPMSAPI.Server.JBPM6.Helpers
{
    public class WorkflowHelper
    {
        public static (string deploymentId, string processDefId) ParseId(string workflowId)
        {
            var data = workflowId.Split('^');
            var deploymentId = data[0];
            var processDefId = data[1];
            return (deploymentId, processDefId);
        }
        
        public static string BuildId(string deploymentId, string processDefId)
        {
            return $"{deploymentId}^{processDefId}";
        }
        
        public static Workflow WorkflowParse(JbpmProcessDefinition processDefinition)
        {
            return new Workflow
            {
                Id = BuildId(processDefinition.DeploymentId, processDefinition.Id),
                Name = processDefinition.Name,
                Version = processDefinition.Version
            };
        }
    }
}