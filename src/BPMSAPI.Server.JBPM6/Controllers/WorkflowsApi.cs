/*
 * BPMS API
 *
 * Универсальный интерфейс взаимодействия с различными BPM системами.
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Swashbuckle.AspNetCore.SwaggerGen;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using BPMSAPI.Server.JBPM6.Attributes;
using BPMSAPI.Server.JBPM6.Helpers;
using BPMSAPI.Server.JBPM6.Models;
using BPMSAPI.Server.SharePoint2007.Helpers;
using JBPM6RESTClient;
using Newtonsoft.Json.Linq;

namespace BPMSAPI.Server.JBPM6.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class WorkflowsApiController : Controller
    {
        private readonly WorkflowService workflowService;

        public WorkflowsApiController(WorkflowService workflowService)
        {
            this.workflowService = workflowService;
        }

        /// <summary>
        /// Получение конкретного процесса.
        /// </summary>
        /// <param name="workflowId">Идентификатор процесса</param>
        /// <response code="200">OK</response>
        /// <response code="204">Процесс не найден</response>
        [HttpGet]
        [Route("/api/workflows/{workflowId}")]
        [ValidateModelState]
        [SwaggerOperation("GetEngineWorkflow")]
        [SwaggerResponse(statusCode: 200, type: typeof(Workflow), description: "OK")]
        [SwaggerResponse(statusCode: 204, type: typeof(Error), description: "Процесс не найден")]
        public virtual async Task<IActionResult> GetEngineWorkflow([FromRoute] [Required] string workflowId)
        {
            try
            {
                var data = WorkflowHelper.ParseId(workflowId);

                var jBpmWorkflow = await workflowService.GetEngineWorkflowAsync(data.deploymentId, data.processDefId);

                if (jBpmWorkflow != null)
                {
                    return StatusCode(200, WorkflowHelper.WorkflowParse(jBpmWorkflow));
                }
                else
                {
                    return StatusCode(204);
                }
            }
            catch (Exception e)
            {
                return StatusCode(400, new Error
                {
                    Message = e.Message,
                    Type = Error.TypeEnum.EngineErrorEnum
                });
            }
        }

        /// <summary>
        /// Получение списка доступных процессов СУБП
        /// </summary>
        /// <param name="pageNum">Номер страницы</param>
        /// <param name="pageSize">Размер страницы</param>
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("/api/workflows")]
        [ValidateModelState]
        [SwaggerOperation("GetEngineWorkflows")]
        [SwaggerResponse(statusCode: 200, type: typeof(WorkflowList), description: "OK")]
        public virtual async Task<IActionResult> GetEngineWorkflowsAsync([FromQuery] int? pageNum,
            [FromQuery] int? pageSize)
        {
            try
            {
                var jBpmWorkflows = await workflowService.GetEngineWorkflowsAsync();
                var workflows = jBpmWorkflows.ProcessDefinitionList.Select(x => WorkflowHelper.WorkflowParse(x.ProcessDefinition));

                var meta = BaseListMetaHelper.GetBaseListMeta(
                    pageNum, 
                    pageSize, 
                    workflows.Count());

                var skip = (meta.PageNum - 1) * meta.PageSize;

                var items = workflows.Skip(skip.Value).Take(meta.PageSize.Value).ToList();
            
                return StatusCode(200, new WorkflowList
                {
                    Meta = meta,
                    Items = items
                });
            }
            catch (Exception e)
            {
                return StatusCode(400, new Error
                {
                    Message = e.Message,
                    Type = Error.TypeEnum.EngineErrorEnum
                });
            }
        }
    }
}